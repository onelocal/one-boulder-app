# One Boulder

![Return to Source](https://one-light.s3.amazonaws.com/static/return_to_source.jpg "Return To Source")

# Getting Started

```
git clone https://gitlab.com/onelocal/one-boulder-app
cd one-boulder-app
npm install
npm link
npm install nodemon -g
serve-flower dev_tom
```
Then go to [localhost:3333](http://localhost:3333) in your browser of choice (Chrome recommended).

# Playing with some code

Go to the code editor of your choice.

The relevant files for just getting started are in the 
```
/app/views (directory)
```
Open /app/views/profile.view

View your profile in the web browser

I recommend having the code and the app view in the same window at the same time, like this.

![docs](https://s3.amazonaws.com/one-light/static/docs.png "docs")

You should see the change happen immediately in your browser.  

When adding a new view (/app/views/[new_view].view), you need to register the view in the conf.json file.

*note* I have not perfected all the error catching yet, so its possible that you may run into a situation where things arent changing or loading.  First fix is reload the page and try again.  If its still broken, there is probably an issue with templates or the logic.  check your developers console to look for messages that may help.

# Using Branches of the Code
Branches allow different versions of code to co-exists and be merge-able with each other.  
Creating a branch
```
git checkout -b [branch_name] 
```
Publishing changes to code repository
```
git add .
git commit -am "[Message to commit, please be as descriptive as possible]"
git push origin [branch_name]
```
Wanting to Publish your own branch and have it accessible in the App? Talk to Juicy about configuring your credentials to give permission for this.

# PHI Framework

PHI is a custom framework built to support rapid evolution and growth.  Many tools are already available and built in to use. While the documentation is happening, my recommendation is to get familiar with the app and see the different types of views and functionality that can be used and their basic configuration.

# Notable aspects of templating / linking
HTML Attributes
action="[event type]:[Function in Context]" - Used to link an event to a function within the context.  EG action="click:alert" will call a this.alert=function(){} within the context of the view if it exists.

link="/route/to/go" - Used to navigate to another page / view within the app.  Routes are made first by the view name, eg event.view gets a route to /event/[event.id].  Additional variables can be passed/used by the view in however they want.

Views are assigned a route when <route></route> tag is present in the the [file_name].view file. The view can then be accessed by using link="/[file_name]".  Or if it is within javascript, you can use app.history.go('/file_name').

Within a view, there are a few command methods to be aware of.
```
this.renderOptions={
	template:'add', //template to initially render for this view.  A view can have multiple templates in it, but this is the first to be rendered
	uid:'add', // a unique ID for this template - it will auto-assign if it is not set
	append:true, // how to add it to the DOM.  Could be append true/false.  Can also use the replace option
	replace: [Dom Element] - if a dom element is passed to replace it will replace the dom element with the newly rendered content
	context:this, // explicitly passing the context (this) connects all the Javascript in the current view to any bindings that happen in the template - generally this will be "this".  
}
this.showOptions={
	display:'page', //['page' (mobile view, page will show sliding from right to left and will animate the page underneath at 1/2 speed.  This also works with scrollable elements to swipe back to previous page),'page_overlay' (on mobile shows from the bottom up and covers the screen),'page_overlay_web' (shows as a modal view in web, animating up from the bottom)]
	renderOnViewReady:1 //if using a this.getPageData(){} to fetch data on page load, this will re-render the view with the data that was fetched.  This will be depreciated at some point.
}
//this is a function to fetch data on the view load from the server.  As it is asyncronous, eg initiating action and some time later return with a result, a callback function is passed (cb) and is called when the necessary data fetching / processing is done.
this.getPageData(cb,reload){
	if(!reload&&self.store.resp&&!self.store.resp.error) return cb();//already have data!
	modules.api({
        url:app.sapiurl+'/module/profile/'+this.options.data.id+'/load',
        data:{},
        timeout:5000,
        callback:function(resp){
        	self.store.resp=resp;
        	cb();
        }
    });
}
//the store is unique to a view and is used to "cache" any relevant data about a view.  This is a persistent store for the view, so when reloading or re-rendering, the data will be saved / used.  Useful for page load data or menu states.
self.store={}
//hooks are event driven listeners based on the life cycle of a view.
this.onInit=function(){} //when the view first loads, do any pre-processing or setting up of the view. First logic to run before anything in the view is processed or rendered.
this.onStart=function(){} // when the view starts / ie becomes visible and active front view
this.onStop=function(){} // when the view goes into the background
this.onResume=function(){} // when a view goes from inactive to active (eg a back navigation will trigger onResume to the view that you are navigating back to).  This also triggers when an app is put to sleep on the phone and then the app comes back into focus.
this.onDestroy=function(){} //any logic needed to clean-up the view before being destroyed
```
By default when using link="" or app.history.go(), the view will render to the default view container based on this logic: (_.isWebLayout())?$('#pages')[0]:$('#wrapper')[0].  _.isWebLayout() is a test that will return what type of mode the app is operating in.  A web layout or a mobile layout.  It is also possible to directly register a view.  This is useful if there are more options that you need to pass than just showing a view.  You do this by calling:
```
phi.registerView('[view_name]',{
	renderTo:$('#wrapper')[0],//where to render to
	data:{//any data you want to pass as options to this view
		id:'UBY0U7L3GFPX'
	}
});
```

bind="" - 
```
bind="loadError:<%=_util.formatOptions({
	ele:'$',
	error:'Not Found',
	onBack:function(){
		this.goBack()
	},
	onRetry:function(){
		var self=this;
		this.getPageData(function(){
            self.refresh();
        });
	}
})%>"
```

References a module to load in the template and the options to pass.  As objects / DOM elements cannot be passed, there is a hydration step that happens, so passing an element is possible by a shorthand method.  EG ele:'$' will pass a jQuery element of the element the component is rendered within.  You can also chain these, like $:.className, where it will first get the parent and then so a search in the DOM for an element with a class "className".


# Notable tools to be aware of (search this project to find instances / usage)
Please view the code itself for documentation until documentation is written.

[modules.infinitescroll](https://gitlab.com/onelocal/one-core/-/blob/master/sites/code/module/infinitescroll/infinitescroll.js) - a general purpose module to support infinitescrolling (in tandem with paginated API).  

[_.](https://gitlab.com/onelocal/one-core/-/blob/master/sites/code/module/tools/tools.js) (Underscore - basic tools and functions)

[modules.moment](https://gitlab.com/onelocal/one-core/-/blob/master/sites/code/module/moment/moment.js) (time formatting and functions)

modules.alertdelegate - gives ability to do nice modal-style alerts.  Extendable options for web and mobile layouts. 

[modules.phone](https://gitlab.com/onelocal/one-core/-/blob/master/sites/code/module/phone/phone.js) - different functions that are associated with the device being used

[modules.formbuilder](https://gitlab.com/onelocal/one-core/-/blob/master/sites/code/module/formbuilder/formbuilder.js) - used to pass a general schema that defines objects that creates a form to get, save, and edit information



