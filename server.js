#!/usr/bin/env nodemon
var watch = require('node-watch');
var express=require('express');
var cors = require('cors')
var exec=require('child_process').exec;
var api=require('./api.js');
var ejsLint = require('ejs-lint');
api.init();
var app = express();
var currentport=(process.argv[3])?process.argv[3]:3333;
app.use(cors({
  origin: 'http://localhost:'+currentport,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}))
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var os = require('os');
var ifaces = os.networkInterfaces();
var local_IP=false;
var env=(process.argv[2])?process.argv[2]:'prod';
if(!env) return console.warn('Environment must be specified');
try{
  var conf=JSON.parse(api.loadFiles(['conf.json']));
}catch(e){
  console.trace();
  return console.warn(e);
}
if(env!='prod') console.log('*****DEV ENVIRONMENT****');
if(!conf) return console.warn('invalid conf.json');
if(!conf.envs||!conf.envs[env]) return console.warn('invalid Environment');
var environment=conf.envs[env];
api.setOptions({
  vars:{
    api:environment.url.replace('https://',''),
    code:environment.url.replace('api','code'),
    prod:environment.prod,
    flower_id:conf.id,
    core:conf.core,
    appid:conf.appid
  }
});
http.listen(currentport, () => {
  console.log('listening on http://localhost:'+currentport+', version 1.2');
});
http.on('error',function(e){
  if(e.message.indexOf('EADDRINUSE')>=0){
        exec('lsof -i tcp:'+currentport, (error, stdout, stderr) => {
          if (error) {
              console.log(`error: ${error.message}`);
              return;
          }
          if (stderr) {
              console.log(`stderr: ${stderr}`);
              return;
          }
         var lines= stdout.split('\n');
         for (var i = 0; i < lines.length; i++) {
           var line=lines[i];
           if(line.indexOf('node')===0){
            var lp=line.replace('  ',' ').replace('  ',' ').replace('  ',' ').split(' ');
            console.log('kill '+lp[1]);
            exec('kill '+lp[1], (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    return;
                }
                console.log('Killed old process - restarting');
                process.exit();
            })
           }
         }
         //console.log(lines)
      });
      }else{
        console.log(e.message)

      }
});// Object.keys(ifaces).forEach(function (ifname) {
//   var alias = 0;
//   ifaces[ifname].forEach(function (iface) {
//     if ('IPv4' !== iface.family || iface.internal !== false) {
//       // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
//       return;
//     }

//     if (alias >= 1) {
//       // this single interface has multiple ipv4 addresses
//       console.log(ifname + ':' + alias, iface.address);
//       console.warn('We have not handled this case yet');
//     }else{
//       // this interface has only one ipv4 adress
//       if(iface.address){
//         console.log(`starting server at http://${iface.address}:${currentport}`)
//        if(currentport==3333) http.listen(currentport,iface.address, () => function(){});
//         currentport++;
//      }
//     }
//     ++alias;
//   });
// });
try{
  var conf=JSON.parse(api.loadFiles(['conf.json']));
}catch(e){
  console.log('Error processing conf.json');
  console.log(e);
  process.exit(0);
}
io.on('connection', (socket) => {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', (data) => {
    console.log(data);
  });
});
//app.use('/static', express.static('static'));
app.get('/app/*',function(req,res){
  try{
      var conf=JSON.parse(api.loadFiles(['conf.json']));
      var path=req.originalUrl.substr(1);
      var pathp=path.split('?');//remove any QS
      var realpath=pathp[0];
      var extp=realpath.split('.');
      var ext=extp[extp.length-1];
      var lp=realpath.split('/');
      var tl=lp[lp.length-1];
      var np=tl.split('.');
      var component=np[0];
      res.set("Access-Control-Allow-Origin", "*");
      res.set("Access-Control-Allow-Headers", "Content-Type,X-Requested-With");
      res.set("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
      switch(ext){
        case 'css':
          res.type("text/css");
        break;
        case 'js':
          res.type("application/javascript");
        break;
        case 'json':
          res.type("application/json");
        break;
        case 'app':
        case 'view':
          res.type("text/plain");
        break;
      }
      if(ext=='app'){
        var options={
          recombine:true,
          script:{
            wrap:{
              start:"phi.loadApp('"+conf.id+"',function(options){",
              end:"})"
            }
          }
        }
      }
      if(ext=='view'){
        var options={
          recombine:true,
          script:{
            wrap:{
              start:"phi.loadView('"+conf.id+"','"+component+"',function(options){",
              end:"})"
            }
          },
          route:{
            wrap:{
              start:"phi.loadRoute('"+conf.id+"','"+component+"',function(options){",
              end:"})"
            }
          },
          dependencies:{
            transform:'dependencies',
            wrap:{
              start:"phi.loadDependencies('"+conf.id+"','"+component+"','",
              end:"')"
            }
          }
        }
        var options=api.getViewConfig(conf,component,{
          recombine:true
        })
      }
      res.send(api.getComponentFiles(false,realpath,options,conf))
      //res.send(api.loadFiles([realpath],api.getConfVars(conf)))
  }catch(e){
     res.send(JSON.stringify({'error':'error: '+e.message}));
  }
});
app.use('/dist', express.static('dist'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});
app.get('/ping', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.send(JSON.stringify({'success':true}))
})
app.get('/conf', (req, res) => {
    try{
      var conf=JSON.parse(api.loadFiles(['conf.json']));
      var appEntry=conf.appEntry;
      appEntry.app=conf.id;
      var fileconf=api.parseConf2(conf);
      res.send(JSON.stringify({'success':true,'conf':{
            "key":conf.id,
            "code":fileconf,
            "entry":conf.entry,
            "appEntry":appEntry
          }
      }));
    }catch(e){
      res.send(JSON.stringify({'error':'error: '+e.message}));
    }
})
app.get('*', function(req, res){
  res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Headers", "Content-Type,X-Requested-With");
    res.set("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.type("text/html");
    res.send(api.loadFiles(['app.html'],api.getConfVars(conf)))
});
var files2={
  'app':'app'
}
for (var prop in files2) {
  var file=files2[prop];
  watch(file, { recursive: true },function(event, filename){
    console.log(event+ ' '+filename);
    var extp=filename.split('.');
    var ext=extp[extp.length-1];
    io.emit('dev_channel',{
        type:'fileupdate',
        data:{
          app:conf.id,
          file:filename,
          ext:ext
        }
      });
  });
}